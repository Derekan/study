#include <iostream>
#include <iomanip> 
#include "windows.h"

using namespace std;

struct gadget
{
    char name[20];
    unsigned  int count;
    float price;
    char article[20];
};

int main()
{
    gadget t[5] = { {"Mouse", 15, 550, "ED33FD43F"},
                   {"Monitor", 20, 999.9, "EG39F343F"},
                   {"Keyboard", 34, 799, "EDD3FG43F"},
                   {"RAM", 26, 1599.9, "ED3FFD4RF"},
                   {"CPU", 31, 4500, "ED53GD73D"} };
    
    cout << setw(10) << "Name" << setw(10) << "Count" << setw(10) << "Price" << setw(15) << "Article" << endl << endl;
    float mult[10];
    double sum = 0;
    for (int i = 0; i < 5; i++)
    {
        cout << setw(10) << t[i].name << setw(10) << t[i].count << setw(10) << t[i].price << setw(15) << t[i].article << endl;
        mult[i] = (t[i].count) * (t[i].price);
    }
    
    sum = mult[0] + mult[1] + mult[2] + mult[3] + mult[4];
    int p, q;

    cout << endl << "the total cost of all goods: " << sum << " UAH" << endl;
    cout << endl << "enter the price range for the product: ";
    cin >> p >> q;
    
    cout << endl << setw(30) << "suitable goods" << endl << setw(10) << "Name" << setw(10) << "Count" << setw(10) << "Price" << setw(15) << "Article" << endl << endl;

    for (int i = 0; i < 5; i++)
    {
        if (t[i].price > p)
            if (t[i].price < q)
                cout << setw(10) << t[i].name << setw(10) << t[i].count << setw(10) << t[i].price << setw(15) << t[i].article << endl;
    }
}